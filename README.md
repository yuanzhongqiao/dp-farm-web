<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 align="center" tabindex="-1" class="heading-element" dir="auto">
    <a target="_blank" rel="noopener noreferrer" href="https://github.com/danielbrendel/hortusfox-web/blob/main/public/logo.png"><img src="https://github.com/danielbrendel/hortusfox-web/raw/main/public/logo.png" width="256" style="max-width: 100%;"></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    霍特斯福克斯
</font></font></h1><a id="user-content---------hortusfox" class="anchor" aria-label="永久链接：HortusFox
" href="#--------hortusfox"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    自托管协作工厂管理系统</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    (C) 2023 - 2024 作者：Daniel Brendel
</font></font></p>
<p align="center" dir="auto">
    <a href="https://www.hortusfox.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">www.hortusfox.com</font></font></a>
</p>
<p align="center" dir="auto">
    <a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/4eb4bf45eb799b1a355fb2b6cc74f45317637fc046b66cd8c5619497e09456a0/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f7765622d7068702d6f72616e6765"><img src="https://camo.githubusercontent.com/4eb4bf45eb799b1a355fb2b6cc74f45317637fc046b66cd8c5619497e09456a0/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f7765622d7068702d6f72616e6765" alt="网页 PHP" data-canonical-src="https://img.shields.io/badge/web-php-orange" style="max-width: 100%;"></a>
    <a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/ebc9b2412f771fe8b66074679d2ab9631921d6fe20b950a4cb07a9d3ce41cb0e/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f64622d6d7973716c2d70696e6b"><img src="https://camo.githubusercontent.com/ebc9b2412f771fe8b66074679d2ab9631921d6fe20b950a4cb07a9d3ce41cb0e/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f64622d6d7973716c2d70696e6b" alt="数据库mysql" data-canonical-src="https://img.shields.io/badge/db-mysql-pink" style="max-width: 100%;"></a>
    <a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/b907b31fbb6f2b505d40105c2db8b2288d5b47d9543579276e38c751ba175369/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c6963656e73652d4d49542d626c7565"><img src="https://camo.githubusercontent.com/b907b31fbb6f2b505d40105c2db8b2288d5b47d9543579276e38c751ba175369/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c6963656e73652d4d49542d626c7565" alt="许可证-MIT" data-canonical-src="https://img.shields.io/badge/license-MIT-blue" style="max-width: 100%;"></a>
    <a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/62f7b2c036e1c64497211a657f0cced032c587c1cbb273fbd6cbd38c0898d029/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6d61696e7461696e65642d7965732d677265656e"><img src="https://camo.githubusercontent.com/62f7b2c036e1c64497211a657f0cced032c587c1cbb273fbd6cbd38c0898d029/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6d61696e7461696e65642d7965732d677265656e" alt="维持-是" data-canonical-src="https://img.shields.io/badge/maintained-yes-green" style="max-width: 100%;"></a>
</p>
<p align="center" dir="auto">
    <a href="https://discord.gg/kc6xGmjzVS" rel="nofollow"><img src="https://camo.githubusercontent.com/1bd9534d3f3c952c340efdf32e4d590ffebf6acc18f6e23df67d4ad9d5862b98/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f646973636f72642d3537313542413f7374796c653d666f722d7468652d6261646765266c6f676f3d646973636f7264266c6f676f436f6c6f723d7768697465" alt="社会不和谐" data-canonical-src="https://img.shields.io/badge/discord-5715BA?style=for-the-badge&amp;logo=discord&amp;logoColor=white" style="max-width: 100%;"></a>
    <a href="https://www.reddit.com/r/hortusfox/" rel="nofollow"><img src="https://camo.githubusercontent.com/e694bbfb48f9ce8dc2d96344bab72f852dc566df3955c96847db2e3e532b1716/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f7265646469742d4431353530413f7374796c653d666f722d7468652d6261646765266c6f676f3d726564646974266c6f676f436f6c6f723d7768697465" alt="社交 Reddit" data-canonical-src="https://img.shields.io/badge/reddit-D1550A?style=for-the-badge&amp;logo=reddit&amp;logoColor=white" style="max-width: 100%;"></a>
    <a href="https://www.youtube.com/@HortusFox" rel="nofollow"><img src="https://camo.githubusercontent.com/36e2dc0fed48fefa69095f93a9ecc69035df2712571406045c65e7c5befe6dc9/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f796f75747562652d7265643f7374796c653d666f722d7468652d6261646765266c6f676f3d796f7574756265266c6f676f436f6c6f723d7768697465" alt="社交 YouTube" data-canonical-src="https://img.shields.io/badge/youtube-red?style=for-the-badge&amp;logo=youtube&amp;logoColor=white" style="max-width: 100%;"></a>
    <a href="https://www.instagram.com/hortusfox/" rel="nofollow"><img src="https://camo.githubusercontent.com/f234fed053e1c5f758e83fe49b5fa14ae18f0d0601d2122edc077e9b0f020c3a/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f696e7374616772616d2d626c75653f7374796c653d666f722d7468652d6261646765266c6f676f3d696e7374616772616d266c6f676f436f6c6f723d7768697465" alt="社交 Instagram" data-canonical-src="https://img.shields.io/badge/instagram-blue?style=for-the-badge&amp;logo=instagram&amp;logoColor=white" style="max-width: 100%;"></a>
</p>
<p align="center" dir="auto">
    <a href="https://ko-fi.com/C0C7V2ESD" rel="nofollow"><img height="36" style="height: 36px; max-width: 100%;" src="https://camo.githubusercontent.com/8993a45baf48465bc724920cd2d508aefd24a278e5d1c4cfd8ba95ac8cb20163/68747470733a2f2f73746f726167652e6b6f2d66692e636f6d2f63646e2f6b6f6669322e706e673f763d33" border="0" alt="在 ko-fi.com 给我买杯咖啡" data-canonical-src="https://storage.ko-fi.com/cdn/kofi2.png?v=3"></a>
</p>
<p align="center" dir="auto">
    <a target="_blank" rel="noopener noreferrer" href="/danielbrendel/hortusfox-web/blob/main/app/resources/gfx/screenshot-desktop.png"><img src="/danielbrendel/hortusfox-web/raw/main/app/resources/gfx/screenshot-desktop.png" style="max-width: 100%;"></a><br>
</p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></h2><a id="user-content-table-of-contents" class="anchor" aria-label="固定链接：目录" href="#table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="#description"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述</font></font></a></li>
<li><a href="#features"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></a></li>
<li><a href="#resources"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">资源</font></font></a></li>
<li><a href="#installation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a>
<ul dir="auto">
<li><a href="#docker"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">码头工人</font></font></a></li>
<li><a href="#installer"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装人员</font></font></a></li>
<li><a href="#manual"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动的</font></font></a></li>
</ul>
</li>
<li><a href="#cronjobs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定时任务</font></font></a></li>
<li><a href="#system-requirements"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统要求</font></font></a></li>
<li><a href="#contributing"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献</font></font></a></li>
<li><a href="#security"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述</font></font></h2><a id="user-content-description" class="anchor" aria-label="永久链接：描述" href="#description"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HortusFox 是一个自托管的协作植物管理系统，您可以在自己的环境中使用它来管理所有植物。您可以添加带有各种详细信息和照片的植物，并将它们分配到您环境中的某个位置。有一个可用的仪表板，显示所有重要的概述信息。该系统还具有警告系统，以指示哪些工厂需要特别护理、用户身份验证、任务、库存管理、日历、协作聊天以及用户所采取操作的历史日志。该系统具有协作管理功能，因此您可以与多个用户一起管理您的工厂。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h2><a id="user-content-features" class="anchor" aria-label="永久链接：特点" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🪴工厂管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🏠 自定义位置</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📜 任务系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📖 库存系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📆 日历系统</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔍 搜索功能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🕰️历史功能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🌦️天气功能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">💬 群聊</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚙️个人资料管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🦋 主题</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🔑 管理仪表板</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📢 提醒</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">💾 备份</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">💻 REST API</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">资源</font></font></h2><a id="user-content-resources" class="anchor" aria-label="永久链接：资源" href="#resources"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://www.hortusfox.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">官方主页</font></font></a></li>
<li><a href="https://hortusfox.github.io/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h2><a id="user-content-installation" class="anchor" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">码头工人</font></font></h3><a id="user-content-docker" class="anchor" aria-label="永久链接：Docker" href="#docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Docker 和 Docker Compose 可以简化设置过程并确保不同环境之间的一致性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">先决条件</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 安装在您的系统上</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker Compose 安装在您的系统上</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按着这些次序：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克隆存储库：</font></font></li>
</ol>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git clone https://github.com/danielbrendel/hortusfox-web.git
<span class="pl-c1">cd</span> hortusfox-web</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git clone https://github.com/danielbrendel/hortusfox-web.git
cd hortusfox-web" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<ol start="2" dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置环境变量在</font></font><code>docker-compose.yml</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置您的管理员用户电子邮件和密码以便登录</font></font></p>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c"><span class="pl-c">#</span> Admin login credentials</span>
<span class="pl-c"><span class="pl-c">#</span> services.app.environment</span>
<span class="pl-ent">APP_ADMIN_EMAIL</span>: <span class="pl-s"><span class="pl-pds">"</span>admin@example.com<span class="pl-pds">"</span></span>
<span class="pl-ent">APP_ADMIN_PASSWORD</span>: <span class="pl-s"><span class="pl-pds">"</span>password<span class="pl-pds">"</span></span></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果需要，设置数据库设置。出于安全原因，鼓励设置自定义密码。所有其他设置可以保持不变。</font></font></p>
<div class="highlight highlight-source-yaml notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c"><span class="pl-c">#</span> Settings used to establish connections to the database</span>
<span class="pl-c"><span class="pl-c">#</span> services.app.environment</span>
<span class="pl-ent">DB_HOST</span>: <span class="pl-s">db</span>
<span class="pl-ent">DB_PORT</span>: <span class="pl-c1">3306</span>
<span class="pl-ent">DB_DATABASE</span>: <span class="pl-s">hortusfox</span>
<span class="pl-ent">DB_USERNAME</span>: <span class="pl-s">user</span>
<span class="pl-ent">DB_PASSWORD</span>: <span class="pl-s">password</span>
<span class="pl-ent">DB_CHARSET</span>: <span class="pl-s"><span class="pl-pds">"</span>utf8mb4<span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Settings of the database container</span>
<span class="pl-c"><span class="pl-c">#</span> services.db.environment</span>
<span class="pl-ent">MYSQL_ROOT_PASSWORD</span>: <span class="pl-s">my-secret-pw</span>
<span class="pl-ent">MYSQL_DATABASE</span>: <span class="pl-s">hortusfox</span>
<span class="pl-ent">MYSQL_USER</span>: <span class="pl-s">user</span>
<span class="pl-ent">MYSQL_PASSWORD</span>: <span class="pl-s">password</span></pre><div class="zeroclipboard-container">
   
  </div></div>
<ol start="3" dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉取镜像并运行应用程序：</font></font></li>
</ol>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker-compose pull
docker-compose up -d</pre><div class="zeroclipboard-container">
    
  </div></div>
<ol start="4" dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="http://localhost:8080" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该应用程序现在应该在http://localhost:8080</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上运行</font><font style="vertical-align: inherit;">。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您现在可以转到</font></font><a href="http://localhost:8080/admin" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理仪表板</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以调整您的工作区设置，也可以转到您的</font></font><a href="http://localhost:8080/profile" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">个人资料页面</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以调整您的用户首选项。</font></font></p>
</li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"></font><i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示：您应该使用与 NTFS不同</font></font></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的文件系统，</font><font style="vertical-align: inherit;">因为它无法正确处理 Unix 所有权、组和权限。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装人员</font></font></h3><a id="user-content-installer" class="anchor" aria-label="永久链接：安装程序" href="#installer"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以使用集成安装程序来安装该产品。为此，请确保您没有通过内部 asatru 开发服务器运行系统。相反，您可能希望从 XAMPP 等 Web 服务器环境的上下文中运行系统。如果这样做，只需在项目的根目录中创建一个名为 do_install（无文件扩展名）的文件，然后浏览到安装程序，系统将引导您完成安装过程。</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>http://localhost/install
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保 PHP 已安装并且您的 Web 服务器和 mysql 服务器正在运行。如果尚未创建供应商文件夹，则系统将尝试运行 Composer 以安装所有必需的依赖项。为此，您需要在系统上安装 Composer。尽管系统会尝试为您创建数据库，但有时这可能会失败，因此您必须在运行安装之前创建数据库。然而，所有表迁移将由系统创建。然后可以通过管理部分管理系统（例如环境设置、用户、位置）。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动的</font></font></h3><a id="user-content-manual" class="anchor" aria-label="永久链接：手册" href="#manual"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了手动安装 HortusFox，您需要首先设置 PHP 环境以及 MySQL 数据库和 Composer。之后您可以克隆或下载存储库。然后进入项目的根目录，让Composer安装所需的依赖项。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>composer install</pre><div class="zeroclipboard-container">
   
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在我们需要配置项目。从 .env.example 创建一个 .env 文件，打开它并管理以下变量：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c"><span class="pl-c">#</span> URL to the official service backend. Used for e.g. version comparision</span>
APP_SERVICE_URL=<span class="pl-s"><span class="pl-pds">"</span>https://www.hortusfox.com<span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> URL to the repository of the project</span>
APP_GITHUB_URL=<span class="pl-s"><span class="pl-pds">"</span>https://github.com/danielbrendel/hortusfox-web<span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> This must be set to true for the product to work in order to enable database connection</span>
DB_ENABLE=true

<span class="pl-c"><span class="pl-c">#</span> Enter your hostname or IP address of your MySQL database server</span>
DB_HOST=localhost

<span class="pl-c"><span class="pl-c">#</span> Enter the database username</span>
DB_USER=root

<span class="pl-c"><span class="pl-c">#</span> Enter the database user password</span>
DB_PASSWORD=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Database connection port. Normally this doesn't need to be changed</span>
DB_PORT=3306

<span class="pl-c"><span class="pl-c">#</span> The actual database of your MySQL server to be used</span>
DB_DATABASE=hortusfox

<span class="pl-c"><span class="pl-c">#</span> Database driver. This needs to be unaltered for now</span>
DB_DRIVER=mysql

<span class="pl-c"><span class="pl-c">#</span> The name of the e-mail sender</span>
SMTP_FROMNAME=<span class="pl-s"><span class="pl-pds">"</span>Test<span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> The e-mail address of the sender</span>
SMTP_FROMADDRESS=<span class="pl-s"><span class="pl-pds">"</span>test@domain.tld<span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Hostname or address to your SMTP mail provider</span>
SMTP_HOST=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Port to be used for connecting to the host</span>
SMTP_PORT=587

<span class="pl-c"><span class="pl-c">#</span> Your SMTP username</span>
SMTP_USERNAME=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Your SMTP password for authentication</span>
SMTP_PASSWORD=<span class="pl-s"><span class="pl-pds">"</span><span class="pl-pds">"</span></span>

<span class="pl-c"><span class="pl-c">#</span> Communication encryption</span>
SMTP_ENCRYPTION=tls</pre><div class="zeroclipboard-container">
 
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保存文件后，您现在可以让产品通过以下命令创建所有必要的表：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>php asatru migrate:fresh</pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">接下来你应该让系统添加所有默认的日历类</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>php asatru calendar:classes</pre><div class="zeroclipboard-container">
  
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在您需要将初始应用程序设置配置文件插入数据库。稍后可以在管理仪表板中调整这些设置。</font></font></p>
<div class="highlight highlight-source-sql notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">INSERT INTO</span> <span class="pl-s"><span class="pl-pds">`</span>AppModel<span class="pl-pds">`</span></span> (id, workspace, language, scroller, chat_enable, chat_timelimit, chat_showusers, chat_indicator, chat_system, history_enable, history_name, enable_media_share, cronjob_pw, overlay_alpha, smtp_fromname, smtp_fromaddress, smtp_host, smtp_port, smtp_username, smtp_password, smtp_encryption, pwa_enable, owm_enable, owm_api_key, owm_latitude, owm_longitude, owm_unittype, owm_cache, created_at) <span class="pl-k">VALUES</span> (
    <span class="pl-k">NULL</span>, 
    <span class="pl-s"><span class="pl-pds">'</span>My workspace name<span class="pl-pds">'</span></span>, 
    <span class="pl-s"><span class="pl-pds">'</span>en<span class="pl-pds">'</span></span>, 
    <span class="pl-c1">1</span>, 
    <span class="pl-c1">1</span>, 
    <span class="pl-c1">5</span>, 
    <span class="pl-c1">1</span>, 
    <span class="pl-c1">0</span>,
    <span class="pl-c1">1</span>, 
    <span class="pl-c1">1</span>, 
    <span class="pl-s"><span class="pl-pds">'</span>History<span class="pl-pds">'</span></span>, 
    <span class="pl-c1">0</span>, 
    <span class="pl-s"><span class="pl-pds">'</span>a-secret-pw<span class="pl-pds">'</span></span>, 
    <span class="pl-k">null</span>, 
    <span class="pl-s"><span class="pl-pds">'</span><span class="pl-pds">'</span></span>, 
    <span class="pl-s"><span class="pl-pds">'</span><span class="pl-pds">'</span></span>, 
    <span class="pl-s"><span class="pl-pds">'</span><span class="pl-pds">'</span></span>, 
    <span class="pl-c1">587</span>, 
    <span class="pl-s"><span class="pl-pds">'</span><span class="pl-pds">'</span></span>, 
    <span class="pl-s"><span class="pl-pds">'</span><span class="pl-pds">'</span></span>, 
    <span class="pl-s"><span class="pl-pds">'</span>tls<span class="pl-pds">'</span></span>,
    <span class="pl-c1">0</span>,
    <span class="pl-c1">0</span>,
    <span class="pl-k">null</span>,
    <span class="pl-k">null</span>,
    <span class="pl-k">null</span>,
    <span class="pl-s"><span class="pl-pds">'</span>default<span class="pl-pds">'</span></span>,
    <span class="pl-c1">300</span>,
    <span class="pl-c1">CURRENT_TIMESTAMP</span>
)</pre><div class="zeroclipboard-container">
 
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您现在可能想要启动 Web 服务器来托管应用程序。如果您想快速使用内置的网络服务器，您可以通过以下方式启动它：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>php asatru serve</pre><div class="zeroclipboard-container">
  
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在浏览到</font></font><a href="http://localhost:8000/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://localhost:8000/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，您应该被重定向到身份验证页面。此时您需要创建您的第一个用户。转到数据库控制面板并切换到用户表。添加应具有管理员权限访问应用程序的用户帐户。下面是一个例子：</font></font></p>
<div class="highlight highlight-source-sql notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">INSERT INTO</span> <span class="pl-s"><span class="pl-pds">`</span>users<span class="pl-pds">`</span></span> (<span class="pl-s"><span class="pl-pds">`</span>name<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>email<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>password<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>admin<span class="pl-pds">`</span></span>) <span class="pl-k">VALUES</span>
(
    <span class="pl-s"><span class="pl-pds">'</span>Username<span class="pl-pds">'</span></span>,
    <span class="pl-s"><span class="pl-pds">'</span>name@example.com<span class="pl-pds">'</span></span>,
    <span class="pl-s"><span class="pl-pds">'</span>your_password_token_here<span class="pl-pds">'</span></span>,
    <span class="pl-c1">1</span>
);</pre><div class="zeroclipboard-container">
   
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可能已经注意到，需要自定义的值是名称、电子邮件、密码和管理员。所有其他值均保留默认值。密码哈希必须手动创建。出于测试目的，您可能只想快速使用以下内容：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>php -r <span class="pl-s"><span class="pl-pds">"</span>echo password_hash('test', PASSWORD_BCRYPT);<span class="pl-pds">"</span></span></pre><div class="zeroclipboard-container">
     
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在，您可以使用您的电子邮件地址和已将哈希值存储在表中的密码，使用初始管理员用户帐户登录。登录后，您应该会被重定向到仪表板。现在可以通过管理区域创建更多用户。用户可以在个人资料首选项中更改密码。他们还可以重置密码。因此，我们将向他们发送一封包含恢复说明的电子邮件。每个新创建的用户都会收到一封确认电子邮件，其中包含自动生成的密码，以便登录。建议用户在首次登录后更改密码。最后但并非最不重要的一点是，您需要将本地环境的所有位置添加到数据库中。您可以通过管理部分或通过将条目手动插入位置表来执行此操作。</font></font></p>
<div class="highlight highlight-source-sql notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">INSERT INTO</span> <span class="pl-s"><span class="pl-pds">`</span>locations<span class="pl-pds">`</span></span> (<span class="pl-s"><span class="pl-pds">`</span>id<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>name<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>icon<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>active<span class="pl-pds">`</span></span>, <span class="pl-s"><span class="pl-pds">`</span>created_at<span class="pl-pds">`</span></span>) <span class="pl-k">VALUES</span>
(
    <span class="pl-k">NULL</span>,
    <span class="pl-s"><span class="pl-pds">'</span>Name of location<span class="pl-pds">'</span></span>,
    <span class="pl-s"><span class="pl-pds">'</span>fas fa-leaf<span class="pl-pds">'</span></span>,
    <span class="pl-c1">1</span>,
    <span class="pl-c1">CURRENT_TIMESTAMP</span>
);</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="INSERT INTO `locations` (`id`, `name`, `icon`, `active`, `created_at`) VALUES
(
    NULL,
    'Name of location',
    'fas fa-leaf',
    1,
    CURRENT_TIMESTAMP
);" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">必填字段是位置名称（例如花园、客厅、厨房等）以及要使用的 FontAwesome 图标。您可以使用所有免费的 FontAwesome 图标（v5.15.4 免费图标）。有关可用图标的完整列表，请访问</font></font><a href="https://fontawesome.com/v5/search?m=free" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FontAwesome 搜索页面</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。请注意，当您以具有管理员权限的用户身份登录时，您可以通过管理部分管理系统的各个方面。此外，您可能想</font><font style="vertical-align: inherit;">为您的用户构建</font></font><a href="https://github.com/danielbrendel/hortusfox-app-android"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Android 移动应用程序。</font></font></a><font style="vertical-align: inherit;"></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定时任务</font></font></h2><a id="user-content-cronjobs" class="anchor" aria-label="永久链接：Cronjobs" href="#cronjobs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cronjobs 用于定期执行特定任务。对于每个 cronjob，您需要使用通过设置的令牌来设置 cronjob 参数</font></font><code>AppModel.cronjob_pw</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。以下 cronjobs 可用：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c"><span class="pl-c">#</span> Used to inform users about overdue tasks. Should be called multiple times per day.</span>
GET /cronjob/tasks/overdue<span class="pl-k">?</span>cronpw={your-auth-token}

<span class="pl-c"><span class="pl-c">#</span> Used to inform users about tasks that are due tomorrow. Should be called multiple times per day.</span>
GET /cronjob/tasks/tomorrow<span class="pl-k">?</span>cronpw={your-auth-token}

<span class="pl-c"><span class="pl-c">#</span> Used to inform users about due calendar dates</span>
GET /cronjob/calendar/reminder<span class="pl-k">?</span>cronpw={your-auth-token}</pre><div class="zeroclipboard-container">
   
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统要求</font></font></h2><a id="user-content-system-requirements" class="anchor" aria-label="永久链接：系统要求" href="#system-requirements"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PHP ^8.2</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MySQL（10.4.27-MariaDB 或类似版本）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于发送电子邮件的 SMTP 服务器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 与 Docker-Compose 用于容器化</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献</font></font></h2><a id="user-content-contributing" class="anchor" aria-label="永久链接：贡献" href="#contributing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您打算为我们的存储库做出贡献，</font><font style="vertical-align: inherit;">请查看</font></font><a href="/danielbrendel/hortusfox-web/blob/main/CONTRIBUTING.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献指南。</font></font></a><font style="vertical-align: inherit;"></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></h2><a id="user-content-security" class="anchor" aria-label="永久链接：安全" href="#security"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您发现任何安全漏洞，请参阅我们的</font></font><a href="/danielbrendel/hortusfox-web/blob/main/SECURITY.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全指南</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以了解如何继续操作。</font></font></p>
</article></div>
